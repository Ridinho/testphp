<?php
$AideGen="<div id=\"Aide-Gen\" style=\"display:none;background-color: #bbbbbb;border: solid 1px red;max-height: 350px;overflow-y: scroll;\">
<fieldset>
<legend style=\"align: top;color: blue;font-weight: bold;\" >Généralités</legend>
<table>
<tr><td><img src=\"./Icones/Aide.png\" style=\"width:35px; height=35px;\"></td>
<td>Fait apparaitre ou masque cet encart.</td></tr>
<tr><td><img src=\"./Icones/Valider.png\" style=\"width:35px; height=35px;\"></td>
<td>Valide les choix effectués pour l'action proposée.</td></tr>
<tr><td><img src=\"./Icones/Annuler.png\" style=\"width:35px; height=35px;\"></td>
<td>Annule l'action envisagée.</td></tr>
<tr><td><img src=\"./Icones/Supprimer.png\" style=\"width:35px; height=35px;\"></td>
<td>Supprime l'élément auquel il est rattaché.</td></tr>
</table></fieldset>
<fieldset>
<legend style=\"align: top;color: blue;font-weight: bold;\" >Les menus</legend>
<p>Le menu principal comporte une option Fichiers non modifiable qui permet de modifier à loisir
les autres options.</p>
<p>On peut  ainsi créer des options du menu donnant accès à des liens sur des pages internet prédéfinies 
ou creer des fichiers utilisés souvent (Nouveau) ou des fichiers rattachés à une option spécifique.</p>
<p>Le fichier des menus apparaît comme un tableau à 2 colonnes. La première est l'intitulé de l'option, 
la deuxième est soit une adresse html soit le nom d'un fichier qui sera créer automatiquement.</p>
<p>Ces options constituent le niveau 2 du menu principal et le nom de
la page constitue l'intitulé de rattachement au menu principal.</p>
</fieldset>
<fieldset>
<legend style=\"align: top;color: blue;font-weight: bold;\" >Les utilisateurs</legend>
<p>Par defaut il n'y a q'un seul utilisateur mais on peut en rajouter autant qu'on veut. Chaque utilisateur 
dispose de son propre espace, menus et fichiers.</p>
<p>Le changement d'utilisateur s'effectue  en cliquant sur le nom de celui en cours (à côté de Bienvenu).</p>
</fieldset>
</div>";

$AidePage="\n<div id=\"Aide-Nav\" style=\"display:none;background-color: #bbbbbb;border: solid 1px red;max-height: 350px;overflow-y: scroll;\">
<fieldset>
<legend style=\"align: top;color: blue;font-weight: bold;\" >Généralités</legend>
<table>
<tr><td><img src=\"./Icones/Aide.png\" style=\"width:35px; height=35px;\"></td>
<td>Fait apparaitre ou masque cet encart.</td></tr>
<tr><td style=\"background-color:#aaaaaa;\">Fichier</td>
<td>Cliquer sur l'intitulé du fichier pour passer en mode modification.</td></tr>
<tr><td style=\"background-color:#cccccc;\">Page</td>
<td>Cliquer sur l'intitulé d'une page pour passer en mode modification.</td></tr>
<tr><td style=\"background-color:#dddddd;\">Ligne</td>
<td>Cliquer sur une ligne pour la sélectionner.</td>
<tr><td colspan=\"2\">Les icones ne sont visibles que lorsqu'elle ont besoin de l'être. </td></tr>
<tr><td colspan=\"2\">Seules les grandes icones donne lieu à une action. Attention la prise en compte est immédiate.</td></tr>
</table></fieldset>
<fieldset>
<legend style=\"align: top;color: blue;font-weight: bold;\" >Modification fichier</legend>
<table>
<tr><td colspan=\"2\">On peut y modifier l'intitulé pour le menu et le titre à afficher. </td></tr>
<tr><td><img src=\"./Icones/Sauvegarder.png\" style=\"width:35px; height=35px;\"></td>
<td>Effectue une sauvegarde du fichier en cours.</td><tr>
<tr><td><img src=\"./Icones/Restaurer.png\" style=\"width:35px; height=35px;\"></td>
<td>Remplace le fichier par sa dernière sauvegarde. .</td><tr>
</tr></table></fieldset>
<fieldset>
<legend style=\"align: top;color: blue;font-weight: bold;\" >Barre de navigation des pages</legend>
<table>
<tr><td><img src=\"./Icones/Replier.png\" style=\"width:35px; height=35px;\"></td>
<td>Permet de passer du mode 'voir toute pages' au mode 'voir pages sélectionnées'.</td><tr>
<tr><td><img src=\"./Icones/Deplier.png\" style=\"width:35px; height=35px;\"></td>
<td>Permet de revenir au mode 'voir toutes pages'.</td><tr>
<tr><td><img src=\"./Icones/Pages.png\" style=\"width:35px; height=35px;\"></td>
<td>Permet de cumuler la visualisation des pages sélectionnées.</td><tr>
<tr><td><img src=\"./Icones/Page.png\" style=\"width:35px; height=35px;\"></td>
<td>Retour à la visualisation d'une seule page.</td><tr>
<tr><td><img src=\"./Icones/PageFusion.png\" style=\"width:35px; height=35px;\"></td>
<td>Fusionne les pages sélectionnées.</td><tr>
<tr><td><img src=\"./Icones/PagePlusAvant.png\" style=\"width:35px; height=35px;\"></td>
<td>Pour inserer une page avant la page sélectionnée.</td></tr>
<tr><td><img src=\"./Icones/PagePlusApres.png\" style=\"width:35px; height=35px;\"></td>
<td>Pour inserer une page après la page sélectionnée.</td></tr>
<tr><td><img src=\"./Icones/PageDecalAvant.png\" style=\"width:35px; height=35px;\"></td>
<td>Pour décaler la page sélectionnée vers le début.</td></tr>
<tr><td><img src=\"./Icones/PageDecalApres.png\" style=\"width:35px; height=35px;\"></td>
<td>Pour décaler la page sélectionnée vers la fin.</td></tr>
</table></fieldset>
<fieldset>
<legend style=\"align: top;color: blue;font-weight: bold;\" >Modification de page</legend>
<table>
<tr><td colspan=\"2\">La première barre supérieure permet de definir les couleurs de fond et les bordures pour la page
 ainsi que le wrap (extension horizontale ou verticale des colonnes en cas de dépassement).</td></tr>
<tr><td colspan=\"2\">Pour la couleur des bordures, il s'agit d'un choix à défilement lors du click.</td></tr>
<tr><td colspan=\"2\">Pour la couleur de fond, clicker sur le champ pour le sélectionner puis sur la couleur prédéfinie pour l'appliquer.
 Pour le choix libre (#cccccc), modifier la valeur puis procéder de la même façon.</td></tr>
<tr><td colspan=\"2\">La deuxième barre permet de definir les styles du texte pour le titre et pour chaque colonne.</td></tr>
<tr><td colspan=\"2\">Le fonctionnement est le même que pour la barre précédente. </td></tr>
<tr><td colspan=\"2\">Ces styles seront appliqués après validation.<img src=\"./Icones/Valider.png\" style=\"width:35px; height=35px;\"></td></tr>
<tr><td><img src=\"./Icones/LigneEnColonne.png\" style=\"width:35px; height=35px;\"></td>
<td>Pour inverser le tableau (ligne en colonne).</td></tr>
<tr><td><img src=\"./Icones/PageDecalAvant.png\" style=\"width:35px; height=35px;\"></td>
<td>Pour décaler la première colonne à la fin.</td></tr>
<tr><td><img src=\"./Icones/PageDecalApres.png\" style=\"width:35px; height=35px;\"></td>
<td>Pour décaler la colonne apres la suivante.</td></tr>
<tr><td><img src=\"./Icones/Supprimer.png\" style=\"width:25px; height=25px;\">
<input type=\"checkbox\" readonly style=\"width:25px;height:25px;\"></td>
<td>Case à cocher pour la suppression de la colonne (données incluses) après validation. </td></tr>
<tr><td><input style=\"width:25px;height:25px;vertical-align:top;font-weight:bold;text-align:center;background-color:#eeeeee;\" type=\"text\" value=\"n\" readonly>
<td>Permet de visualiser la ou les somme(s) sur la colonne en fonction du groupement.</td></tr>
<tr><td><input type=\"checkbox\" readonly style=\"width:25px;height:25px;\">
<img src=\"./Icones/Ajouter.png\" style=\"width:25px; height=25px;\"></td>
<td>Case à cocher pour l'ajout d'une colonne par duplication à cet endoit après validation.</td></tr>
<tr><td colspan=\"2\">Les attributs de colonne en grouper décalé sont ceux de la première ligne du groupe.</td></tr>
<tr><td><img src=\"./Icones/Imprimer.png\" style=\"width:45px; height=45px;\"></td>
<td>Affiche juste la page dans un nouvel onglet pour être Imprimee avec le navigateur.</td><tr>
</table></fieldset>
<fieldset>
<legend style=\"align: top;color: blue;font-weight: bold;\" >Modification de ligne</legend>
<table>
<tr><td colspan=\"2\">La barre supérieure permet de definir les styles du texte pour le titre et pour chaque cellule.
sont fonctionnement est le même que pour la modification de page.</td></tr>
<tr><td colspan=\"2\">Ces styles seront appliqués après validation.<img src=\"./Icones/Valider.png\" style=\"width:35px; height=35px;\"></td></tr>
<tr><td colspan=\"2\">Le style peut également être appliqué à la ligne entière (cad à chaque cellule).</td></tr>
<tr><td><img src=\"./Icones/PageScinder.png\" style=\"width:35px; height=35px;\"></td>
<td>Scinder la page sur la ligne sélectionnée.</td><tr>
<tr><td><img src=\"./Icones/LignePlusAvant.png\" style=\"width:35px; height=35px;\"></td>
<td>Inserer une ligne avant la ligne sélectionnée.</td><tr>
<tr><td><img src=\"./Icones/LignePlusApres.png\" style=\"width:35px; height=35px;\"></td>
<td>Inserer une ligne après la ligne sélectionnée.</td><tr>
<tr><td colspan=\"2\">Pour modifier la ligne d'entete il faut d'abord decocher l'option, modifier et la remettre.</td></tr>
<tr><td colspan=\"2\">Pour la très mini calculette, les nombres décimaux sont à saisir avec un point.</td></tr>
</table></fieldset>
</div>";
?>