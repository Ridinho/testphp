<?php
//Format fichier menus
//Niveau 1: une option = Intitulé de page
//Niveau 2: Une ligne(colonne 1) = une option du menu
//Action= (colonne 2) Href ou affichage de fichier
$TableMnu=array();
$IdxMenus=array();
// Récupération du fichier
function RecupMenus() {
	global $TableMnu,$IdxMenus,$Qui,$Menu;
	$FichierMnu="./".$Qui."/Menu-Main.csv";
	$FichierInf="./".$Qui."/Menu-Main.inf";
	$Hm = fopen($FichierMnu, "r");
	$Hi = fopen($FichierInf, "r");
	$i=0;
	if ($Hm) {
		$NbrLignes = 0;
		while (!feof($Hm)) {
		   	$tmp1=fgets($Hm, 1024);
		   	if($tmp1 === false) break;
		   	$tmp1=rtrim($tmp1);
	    	$TableMnu[$NbrLignes] = $tmp1;
	    	$tmp2=fgets($Hi, 1024);
	    	$tmp2=rtrim($tmp2);
	    	$ya=strpos($tmp2,'NP');
	    	if ($ya===0) {
		    	$IdxMenus[$i]=$NbrLignes;
		    	$i++;
	    	}
	    	$NbrLignes++;
	 	}
	}
	fclose($Hm);
	fclose($Hi);
}
// pour la liste des fichiers
function RecupListe ($num) {
	global $Qui,$Admin;
	$ret="\n<div id=\"AffMnu-$num\" name=\"AffMnu\" class=\"Menu\" style=\"display:none;border-top:solid 1px red;\"><center>
	<table><tr>";
	$ListeCsv=glob('./'.$Qui.'/*.csv');
	$nbr=count($ListeCsv);
	for($i=0; $i<$nbr; $i++) {
	// pour garder que les noms
		$ListeCsv[$i]=str_replace("./$Qui/","",$ListeCsv[$i]);
		$ListeCsv[$i]=str_replace(".csv","",$ListeCsv[$i]);
		if ($ListeCsv[$i] != "Menu-Main") {
			$Action="Aff;Main;$ListeCsv[$i]";
			$ret.="<td><input type=\"button\" class=\"Option_N2\" onclick=\"Aff_Menu(0,'$Action');\" value=\"$ListeCsv[$i]\"></td>";
		}
	}
	$ret.="\n<td><input type=\"button\" class=\"Option_N2\" onClick=\"document.forms[0].QuoiVeux.value='DCF;Nouveau;0';document.forms[0].submit();\" value=\"Nouveau\"></td>
	<td><input type=\"button\" class=\"Option_N2\" onClick=\"document.forms[0].QuoiVeux.value='Aff;Menu-Main';document.forms[0].submit();\" value=\"Menus\"></td>";
	if ($Qui == $Admin) {
		$ret.="\n<td><input type=\"button\" class=\"Option_N2\" onClick=\"document.forms[0].QuoiVeux.value='DMU;0';document.forms[0].submit();\" value=\"Utilisateurs\"></td>";
	}
	$ret.="</tr></table></center></div>";
	return $ret;
}
// pour le menu principal
function MefMenu() {
	global $TableMnu,$IdxMenus,$Menus,$Menu;
	RecupMenus();
	$Niv2="";
	$num=1;
	$Idx=0;
	$Niv1="\n<div id=\"AffMnu-0\" name=\"AffMnu\" class=\"Menu\" style=\"border-top:solid 1px red;\">
	<table><tr>";
	// pour le Menu Fichier
	$Niv2.=RecupListe($num);
	$Niv1.="\n<td><input type=\"button\" class=\"Option_N1\" id=\"ModBtn-$num\" name=\"ModBtn\" onclick=\"Aff_Menu($num,'N2');\" value=\"Fichiers\"></td>";
	// pour les autre menus
	$num++;
	for($i=1; $i<count($TableMnu); $i++) {
		if ($i == $IdxMenus[$Idx])  {
			$Niv1.="\n<td><input type=\"button\" class=\"Option_N1\" id=\"ModBtn-$num\" name=\"ModBtn\" onclick=\"Aff_Menu($num,'N2');\" value=\"$TableMnu[$i]\"></td>";
			if ($i > 1) {
				$Niv2.="</tr></table></center></div>";
			}
			$Niv2.="\n<div id=\"AffMnu-$num\" name=\"AffMnu\" class=\"Menu\" style=\"display:none;border-top:solid 1px red;\">
			<center><table><tr>";
			$Idx++;
			$num++;
		} else {
			$tab=explode(";",$TableMnu[$i]);
			if (filter_var($tab[1], FILTER_VALIDATE_URL)) {
				$Action="Href;$tab[1]";
			} else {
				$Action="Aff;Fichiers;$tab[1]";
			}
			$Niv2.="<td><input type=\"button\" class=\"Option_N2\" onclick=\"Aff_Menu(0,'$Action');\" value=\"$tab[0]\"></td>";
		}
	}
	$Niv2.="\n</tr></table></center></div>";
	$Menus= $Niv1."</tr></table></div>".$Niv2;
}
?>