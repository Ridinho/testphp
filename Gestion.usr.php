<?php
$Users=array();
// Récupération des utilisateurs
function RecupUsers() {
	global $Users;
	$handle = fopen("Utilisateurs.csv", "r");
	$i=0;
	while (!feof($handle)) {
	   	$tmp1=fgets($handle, 1024);
	   	if($tmp1 === false) break;
	   	$Users[$i]=rtrim($tmp1);
	   	$i++;
	}
	fclose($handle);
}
// suppression utilisateur
function suprdir($usr) {
	// repertoire Fichiers
	$dir="$usr/Fichiers";
	$files = scandir($dir);
	$nbr=count($files);
  	foreach ($files as $file) {
		if (!is_dir("$dir/$file")) {
	       unlink("$dir/$file");
		}
    }
    rmdir($dir);
    // reprtoire utilisateur
    $files = scandir($usr);
    foreach ($files as $file) {	
	    if (!is_dir("$usr/$file")) {
	    unlink("$usr/$file");
	    }
	}
    rmdir($usr);
}
 // Modification utilisateurs
function MU(&$ActionDmd) {
	global $Users;
	$j=1;
	$next=count($Users);
	$Modif="N";
	for ($i=1; $i<count($Users); $i++) {
		$tmp=explode(";",$Users[$i]);
		$Alias="Alias_$j";
		$User="User_$j";
		if ($_POST[$Alias] == "" && $_POST[$User] == "" && $i != 1) {
			// Suppression
			$SuprUser="./$tmp[1]";
			array_splice($Users,$i,1);
			suprdir($SuprUser); 
			$Modif="O";
		} elseif ($_POST[$Alias] != "" && $_POST[$User] != "") {
			if ($tmp[1] != trim($_POST[$User])) {
				// utilisateur modifié
				$OldUser="./$tmp[1]";
				$NewUser="./$_POST[$User]";
				rename($OldUser,$NewUser);
			}
			$NewVal=trim($_POST[$Alias]).";".trim($_POST[$User]);
			$Users[$i]=$NewVal;
			$Modif="O";
		}
		$j++;	
	}
	// en cas d'ajout
	$Alias="Alias_$next";
	$User="User_$next";
	$i=count($Users);
	if ($_POST[$Alias] != "" && $_POST[$User] != "") {
		$NewDir="./".trim($_POST[$User]);
		mkdir($NewDir,0777);
		$DirFichiers=$NewDir."/Fichiers";
		mkdir($DirFichiers,0777);
		copy("Menu-Main.ref.csv", "$NewDir/Menu-Main.csv"); 
		copy("Menu-Main.ref.inf", "$NewDir/Menu-Main.inf"); 
		$NewVal=trim($_POST[$Alias]).";".trim($_POST[$User]);
		$Users[$i]=$NewVal;
		$Modif="O";
	}
	// on enregistre le nouveau fichier
	if ($Modif == "O") {
		$handle = fopen("Utilisateurs.csv", "w");
		$NbrLignes=count($Users);
		for ($i=0; $i<$NbrLignes; $i++)	{
			if ($i != $NbrLignes) {
				$tmp1=$Users[$i]."\n";
			}
			fwrite($handle,$tmp1);
		}
		fclose($handle);
	}
	$ActionDmd[0]="Aff";
	$ActionDmd[1]="Rien";
	Aff($ActionDmd);
}
// Demande modif des utilisateurs
function DMU(&$ActionDmd) {
	global $Users,$Form;
	$Form.="\n<div id=\"Zone-Ctl\" class=\"Zone-Visu\">
	<div class=\"Titre\">Modification des Utilisateurs</div>
	<table style=\"width:100%;\">
	<tr><td style=\"text-align: center;\">Alias</td><td style=\"text-align: center;\">Utilisateur</td></tr>";
	$j=1;
	for ($i=1; $i<count($Users); $i++) {
		$tmp=explode(";",$Users[$i]);
		$Form.="<tr><td><input type=\"text\" class=\"Saisie\" id=\"Alias_$j\" name =\"Alias_$j\" value=\"$tmp[0]\" style=\"width:100%;text-align:center;\"></td>
		<td><input type=\"text\" class=\"Saisie\" id=\"User_$j\" name =\"User_$j\" value=\"$tmp[1]\" style=\"width:100%;text-align:center;\"></td></tr>";
		$j++;	
	}
	$Form.="<tr><td><input type=\"text\" class=\"Saisie\" id=\"Alias_$j\" name =\"Alias_$j\" value=\"\" style=\"width:100%;text-align:center;\"></td>
	<td><input type=\"text\" class=\"Saisie\" id=\"User_$j\" name =\"User_$j\" value=\"\" style=\"width:100%;text-align:center;\"></td></tr>
	<tr><td style=\"text-align: center;\"><img src=\"./Icones/Valider.png\" onclick=\"document.forms[0].QuoiVeux.value='MU;$ActionDmd[1]';document.forms[0].submit();\" style=\"width:50px; height=50px;\"></td>
	<td style=\"text-align: center;\"><img src=\"./Icones/Annuler.png\" onclick=\"document.forms[0].QuoiVeux.value='Aff;Rien';document.forms[0].submit();\" style=\"width:50px; height=50px;\"></td></tr>
	</table></div>
	<div style=\"background-color: #bbbbbb;border: solid 1px red;max-height: 350px;overflow-y: scroll;\">
	<p>- Cette liste n'a pas besoin d'être ordonnée.</p>
	<p>- Les deux champs Alias et Utilisateur sont obligatoires, le premier est la valeur à saisir pour l'identification et le second sert à creer le répertoire de l'utilisateur.</p>
	<p>- Ces deux champs ne doivent pas comporter d'espaces.</p>
	<p>- Si la ligne vide est correctement remplie, un mouvel utilisateur sera créé.</p>
	<p>- Si deux champs Alias et Utilisateur existants sont vides, l'utilisateur sera supprimé (sauf le premier) ainsi que ses données.</p>
	</div>";
}
?>