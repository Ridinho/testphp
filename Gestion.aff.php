<?php
/////////////////////////////////////////////////////////////
// Fichier csv
// [0] Titre fichier
// [1] Titre page
// [2] Données page
// Fichier inf
// [0] Titre fichier/nbr pages/afficher
// [1] NP/Titre page/mode voir/grouper sur colonne 0/entete colonnes(O/N)/total par colonne (O/N)/style 
// [2] Donnees page
// afficher : par page (P) ou en continu (C) $ModeAff
// non groupe(0) sans decallage(1) avec decallage(2)
$FondVert="#66cc66";	
$FondBleu="#ccccff";	
$FondOrange="#ff9933";
$FondSaumon="#ffcc99";
$FondJaune="#f0e68c";	
$FondGris1="#cccccc";	
$FondGris2="#dddddd";
$ChoixStyle="\n<div style=\"width: 100%;text-align:center;border:solid 1px red;overflow-x: scroll;\"><table style=\"width:100%\"><tr>
<td><img src=\"./Icones/Aligne-Gauche.png\" onclick=\"MajAligne('left')\" style=\"width:45px; height=45px;\"></td>
<td><img src=\"./Icones/Aligne-Centre.png\" onclick=\"MajAligne('center')\" style=\"width:45px; height=45px;\"></td>
<td><img src=\"./Icones/Aligne-Droite.png\" onclick=\"MajAligne('right')\" style=\"width:45px; height=45px;\"></td>
<td><img src=\"./Icones/Texte-Gras.png\" onclick=\"MajGras()\" style=\"width:45px; height=45px;\"></td>
<td><img src=\"./Icones/Texte-Italique.png\" onclick=\"MajStyle()\" style=\"width:45px; height=45px;\"></td>
<td><img src=\"./Icones/Texte-Souligne.png\" onclick=\"MajDeco()\" style=\"width:45px; height=45px;\"></td>
<td><img src=\"./Icones/Texte-Noir.png\" onclick=\"MajCouleur('black')\" style=\"width:45px; height=45px;\"></td>
<td><img src=\"./Icones/Texte-Bleu.png\" onclick=\"MajCouleur('blue')\" style=\"width:45px; height=45px;\"></td>
<td><img src=\"./Icones/Texte-Vert.png\" onclick=\"MajCouleur('green')\" style=\"width:45px; height=45px;\"></td>
<td><img src=\"./Icones/Texte-Rouge.png\" onclick=\"MajCouleur('red')\" style=\"width:45px; height=45px;\"></td>
</tr></table></div>";
$ChoixCoul="<table width= \"100%\"><tr>
<td style=\"border:1px solid black;width:14%;height:25px;background-color:$FondVert;\" onclick=\"MajFond('$FondVert');\">&nbsp</td>
<td style=\"border:1px solid black;width:14%;background-color:$FondBleu;\" onclick=\"MajFond('$FondBleu');\">&nbsp</td>
<td style=\"border:1px solid black;width:14%;background-color:$FondSaumon;\" onclick=\"MajFond('$FondSaumon');\">&nbsp</td>
<td><input type=\"text\" class=\"Saisie\" id=\"FondLibre\" style=\"width:98%;height:25px;font-size:16px;text-align:center\" value=\"#ff9933\" onclick=\"MajFond(this.value);\"></td>
<td style=\"border:1px solid black;width:14%;background-color:$FondJaune;\" onclick=\"MajFond('$FondJaune');\">&nbsp</td>
<td style=\"border:1px solid black;width:14%;background-color:$FondGris1;\" onclick=\"MajFond('$FondGris1');\">&nbsp</td>
<td style=\"border:1px solid black;width:14%;background-color:$FondGris2;\" onclick=\"MajFond('$FondGris2');\">&nbsp</td>
</tr></table>";
// renvoie une page du classeur
function MefTable (&$ActionDmd,$deb,$fin,$Pnum) {
	global $Table, $Infos;
	$tmp=explode(",",$Infos[$deb]);
	$Grouper=$tmp[2];
	$Entete=$tmp[3];
	$Totaux=$tmp[4];
	$totals=explode("-",$Totaux);
	$NbrCol=count($totals);
	$Span=$NbrCol;
	// le tableau si entete
	$debcol=0;
	$debligne=$deb+1;
	if ($Entete == "O") {
		$tab=explode(";",$Table[$debligne]);
		$debligne+=1;
		if ($Grouper == 2) {
			$debcol=1;
		}
		$ret.="\n<tr>";
		for($i=$debcol;$i<$NbrCol;$i ++) {
			$ret.="\n<td style=\"font-weight:800;text-align:center;background-color: #cccccc;\">$tab[$i]</td>";
		}
		$ret.="\n</tr>";
	}
	// le reste du tableau
	$TotDmd="N";
	$rubrique="Debut";
	$n=0;
	for ($i=$debligne;$i<$fin;$i++) {
		$tab=explode(";",$Table[$i]);
		$styles=explode(",",$Infos[$i]);
		if ($Grouper == 0 ) {
			// non groupe
			$ret.="\n<tr class=\"fond-3\" onClick=\"document.forms[0].QuoiVeux.value='DML;$ActionDmd[1];$Pnum;$i';document.forms[0].submit();\">";
			for ($k=0; $k<$NbrCol; $k ++) {
				$tmp=str_replace(' ','',$tab[$k]);
				$tmp=str_replace(',','.',$tmp);
				if ($totals[$k] != "n" && is_numeric($tmp)) {
					$TotDmd="O";
					$Tot[0][$k] = $Tot[0][$k]+$tmp;
				}
				$ret.="\n<td id=\"$i-$k\" style=\"$styles[$k];\">$tab[$k]</td>";
			}
			$ret.="</tr>";
		} elseif ($Grouper == 1) {
			// groupe sans decallage
			$col0="&nbsp";
			if ($tab[0] != $rubrique) {
				if ($i != $debligne && $TotDmd != "N") {
					// on affiche le total precedent
					$ret.="\n<tr class=\"fond-3\">";
					for ($k=0; $k<$NbrCol; $k ++) {
						$ret.="\n<td style=\"color:blue;text-align:right;\">".$Tot[$n][$k]."</td>";
					}
					$ret.="</tr>";
				}
				$n++;
				$rubrique = $tab[0];
				$col0="<b>".$tab[0]."</b>";
				$ya="O";
			}
			$ret.="\n<tr class=\"fond-3\" onClick=\"document.forms[0].QuoiVeux.value='DML;$ActionDmd[1];$Pnum;$i';document.forms[0].submit();\">";
			if ($ya=="O") {
				$ret.="\n<td style=\"$styles[0]\">$tab[0]</td>";
				$ya="N";
			} else {
				$ret.="\n<td></td>";
			}
			for($k=1;$k<$NbrCol;$k ++) {
				$tmp=str_replace(' ','',$tab[$k]);
				$tmp=str_replace(',','.',$tmp);
				if ($totals[$k] != "n" && is_numeric($tmp)) {
					$TotDmd="O";
					$Tot[$n][$k] = $Tot[$n][$k]+$tmp;
				}
				$ret.="\n<td style=\"$styles[$k]\">$tab[$k]</td>";
			}
			$ret.="</tr>";
			// si dernière ligne
			if ($i == ($NbrLigne-1) && $TotDmd == "O") {
				// on affiche le total courant
				$ret.="\n<tr class=\"fond-3\"><td>$col0</td>";
				for ($k=1; $k<$NbrCol; $k ++) {
					$ret.="\n<td style=\"color:blue;text-align:right;\">".$Tot[$n][$k]."</td>";
				}
				$ret.="</tr>";
			}
		} else {
			// groupe avec decallage
			if ($tab[0] != $rubrique) {
				if ($i != $debligne && $TotDmd != "N") {
					// on affiche le total precedent
					$ret.="\n<tr class=\"fond-3\">";
						for ($k=1; $k<$NbrCol; $k ++) {
						$ret.="\n<td style=\"color:blue;text-align:right;\">".$Tot[$n][$k]."</td>";
					}
					$ret.="</tr>";
				}
				$n++;
				$rubrique = $tab[0];
				$ret.="\n<tr class=\"fond-2\"><td style=\"$styles[0]\" colspan=$Span><b>$tab[0]</b></td></tr>";
			} 
			$ret.="\n<tr class=\"fond-3\" onClick=\"document.forms[0].QuoiVeux.value='DML;$ActionDmd[1];$Pnum;$i';document.forms[0].submit();\">";
			for($k=1;$k<$NbrCol;$k ++) {
				$tmp=str_replace(' ','',$tab[$k]);
				$tmp=str_replace(',','.',$tmp);
				if ($totals[$k] != "n" && is_numeric($tmp)) {
					$TotDmd = "O";
					$Tot[$n][$k] = $Tot[$n][$k]+$tmp;
				}
				$ret.="\n<td style=\"$styles[$k]\">$tab[$k]</td>";
			}
			$ret.="</tr>";
			// si dernière ligne
			if ($i == $NbrLigne-1 && $TotDmd == "O") {
				// on affiche le total courant
				$ret.="\n<tr class= \"fond-3\">";
				for ($k=1; $k<$NbrCol; $k ++) {
					$ret.="\n<td style=\"color:blue;text-align:right;\">".$Tot[$n][$k]."</td>";
				}
				$ret.="</tr>";
			}
		}
		$j++;
	}
	// pour le total general
	if ($TotDmd == "O") {
		$Couleur="blue";
		$ret.="\n<tr class=\"fond-2\">";
		$nbr=count($Tot);
		for ($i=0; $i < $nbr; $i++) {
			for ($k=1; $k<$NbrCol; $k ++) {
				if ($totals[$k] != "n") {
					$Tot[$nbr][$k] = $Tot[$nbr][$k] + $Tot[$i][$k];			
				}	
			}
		}
		$deb=0;
		if ($Grouper == 2) {$deb=1;}	
		for ($k=$deb; $k<$NbrCol; $k ++) {
			$ret.="\n<td style=\"font-weight:bold;color:$Couleur;text-align:right;\">".$Tot[$nbr][$k]."</td>";
		}
		$ret.="</tr>";
	}
	return $ret;
	
}
//on affiche le fichier
function Aff (&$ActionDmd) {
	// renvoie toutes les pages du fichier
	global $AidePage,$FondVert,$FondBleu,$NbrLignes,$ChoixStyle,$Table,$Infos,$IdxPages,$Haff,$Form;
	// Retour maison
	if ($ActionDmd[1] == "Rien") {
		// on actualise l'image en visu
		$ListeImages=glob('./Bestof/*.{jpg,png,gif}', GLOB_BRACE);
		$nbr=count($ListeImages)-1;
		$Idx=rand(0,$nbr);
		$UrlImg=$ListeImages[$Idx];
		// on met juste l'image en visu
		$Form.="\n<div style=\"float: right;width: 100%;\">
		<img src=\"$UrlImg\" width=\"100%\">
		</div>";
		return;
	}
	// ya qqch a afficher
	RecupFichiers($ActionDmd);
	// on affiche
	if (isset($ActionDmd[2])) {
		$PageDmd=$ActionDmd[2];
	} else {
		$PageDmd=0;
	}
	$NbrPages=count($IdxPages);
	// affichage en page(n°) ou continu(0)
	if ($PageDmd == 0) {
		$Display="block";
		$Display2="none";
		$Display3="none";
	} else {	
		$Display="none";
		$Display2="block";
		$Display3="none";
	}
	$CtlModeAff="";
	if ($NbrPages > 1) {
		$CtlModeAff="\n<td><img id=\"img-1\" name=\"VoirReplier\" src=\"./Icones/Replier.png\" onclick=\"Mode_Aff(2)\" style=\"display:$Display;width:35px; height=35px;\"></td>
		<td><img id=\"img-2\" name=\"VoirDeplier\" src=\"./Icones/Deplier.png\" onclick=\"Mode_Aff(1)\" style=\"display:$Display2;width:35px; height=35px;\"></td>
		<td><img id=\"img-3\" name=\"VoirPages\" src=\"./Icones/Pages.png\" onclick=\"Mode_Aff(3)\" style=\"display:$Display2;width:35px; height=35px;\"></td>
		<td><img id=\"img-4\" name=\"VoirPage\" src=\"./Icones/Page.png\" onclick=\"Mode_Aff(2)\" style=\"display:$Display3;width:35px; height=35px;\"></td>";
	}
	$tmp=explode(",",$Infos[0]);
	$Form.="\n<div class=\"Titre\" id=\"TitreFichier\"><table width=\"100%\">
	<tr>$CtlModeAff
	<td style=\"font-size:20px;width:80%;$tmp[1]\" onClick=\"document.forms[0].QuoiVeux.value='DMF;$ActionDmd[1]';document.forms[0].submit();\">$tmp[0]</td>
	<td><img id=\"img-5\" name=\"PagePlusAv\" src=\"./Icones/PagePlusAvant.png\" onclick=\"Quoi_Veux('DIP;$ActionDmd[1];Av');\" style=\"display: $Display2;width:40px; height=40px;\"></td>
	<td><img id=\"img-6\" name=\"PagePlusAp\" src=\"./Icones/PagePlusApres.png\" onclick=\"Quoi_Veux('DIP;$ActionDmd[1];Ap');\" style=\"display: $Display2;width:40px; height=40px;\"></td>
	<td><img id=\"img-7\" name=\"PageDecalAv\" src=\"./Icones/PageDecalAvant.png\" onclick=\"Quoi_Veux('DP;$ActionDmd[1];Av');\" style=\"display: $Display2;width:40px; height=40px;\"></td>
	<td><img id=\"img-8\" name=\"PageDecalAp\" src=\"./Icones/PageDecalApres.png\" onclick=\"Quoi_Veux('DP;$ActionDmd[1];Ap');\" style=\"display: $Display2;width:40px; height=40px;\"></td>
	<td><img id=\"img-9\" name=\"PageFusion\" src=\"./Icones/PageFusion.png\" onclick=\"Quoi_Veux('FP;$ActionDmd[1]');\" style=\"display: $Display3;width:40px; height=40px;\"></td>
	<td><img src=\"./Icones/Aide.png\" onclick=\"AffAide('Nav');\" style=\"width:35px; height=35px;\"></td>
	</tr></table></div>";
	$NavPages="\n<div class=\"Menu\" id=\"img-0\" nom=\"NavPages\" style=\"display:$Display2;\"><table><tr>";
	$DML="";
	$Pages="";
	$PageNum=0;
	// on récupère tout page par page
	for ($i=0; $i<$NbrPages; $i++) {
		$PageNum++;
		$IdxVoir++;
		$deb=$IdxPages[$i];
		if ($i == ($NbrPages-1)) {
			$fin=$NbrLignes;
		} else {
			$fin=$IdxPages[$i+1];
		}
		if ($PageDmd != 0) {
			// affichage par page
			$Display="none";
			$Fond=$FondBleu;
			if ($PageNum == $PageDmd) {
				$Display="block";
				$Fond=$FondVert;
			}
		} else {
			// affichage en continu
			$Fond=$FondBleu;
			$Display="block";
		}
		$tmp=explode(",",$Infos[$deb]);
		$StyleTitre="style=\"background-color:#cccccc;font-size:18px;\"";
		if ($tmp[5] != "") {
			$StyleTitre="style=\"background-color:#cccccc;font-size:18px; $tmp[5]\"";
		}
		// si pas titre page
		if ($tmp[1] == "") {
			$TitrePage="Page-$PageNum";
		} else {
			$TitrePage=$tmp[1];
		}
		$nbrcol=count(explode("-",$tmp[4]));
		if ($tmp[7] == "aucune") {
			$Bordures="none";
		} else {
			$Bordures="1px solid $tmp[7]";
		}
		$Wrap="wrap";
		if ($tmp[8] == "non") {
			$Wrap="nowrap";
		}
		$StylePage="\n<style type=\"text/css\">
		.VisuPage_$PageNum {
			min-width: 100%;
			white-space: $Wrap;
			border: none;
			border-collapse: collapse;
			spacing: 0;
			margin: 0;
			padding: 0;}
		.VisuPage_$PageNum td {
			font: normal 16px Arial, Helvetica, sans-serif;
			background-color: $tmp[6];
			border: $Bordures;
			spacing: 0;
			margin: 0;
			padding: 0;}
		</style>";
		$Class="VisuPage_".$PageNum;
		$NavPages.="\n<td><input type=\"button\" class=\"Option_N1\" id=\"BtnPage-$PageNum\" style=\"background-color: $Fond;\" value=\"$TitrePage\" onclick=\"Aff_Page($PageNum);\"></td>";
		$Pages.="\n<div class=\"Zone-Visu\" name=\"Page\" id=\"Page-$PageNum\" style=\"display:$Display;\">$StylePage
		<table class=\"VisuPage_$PageNum\" ><tr><td colspan=\"$nbrcol\"
		$StyleTitre onClick=\"document.forms[0].QuoiVeux.value='DMP;$ActionDmd[1];$PageNum';document.forms[0].submit();\">$TitrePage</td></tr>";
		$Pages.=MefTable($ActionDmd,$deb,$fin,$PageNum);
		$Pages.="\n</table></div>";
	}
	// affichage 
	if ($NbrPages > 1) {
		$Form.=$NavPages."\n</tr></table></div>";
	}
	$Form.="\n$AidePage
	<div class=\"Zone-Visu\" id=\"zone-visu\" style=\"height:$Haff px;\">";
	$Form.=$Pages."\n</div>";
}
// Demande de modification de ligne
function DML (&$ActionDmd) {
	global $ChoixStyle,$Table,$Infos,$IdxPages,$Form;
	RecupFichiers($ActionDmd);
	$IdxDmd=$IdxPages[($ActionDmd[2]-1)];
	$InfosPage=$Infos[$IdxDmd];
	$tmp=explode(",",$InfosPage);
	$TitreCol=explode(";",$Table[$IdxDmd+1]);
	$Idx=$ActionDmd[3];
	$tab=explode(";",$Table[$Idx]);
	$nbrcol=count($tab);
	$Styles=explode(",",$Infos[$Idx]);
	$Form.="\n<div id=\"Zone-Ctl\" class=\"Zone-Visu\"><center>
	<div class=\"Titre\">Modification ligne</div>
	$ChoixStyle
	<table style=\"width:100%;\"><tr><td style=\"width:100%;\">
	<input type=\"text\" class=\"Saisie\" id=\"ch-li\" name=\"ch-li\" style=\"width:100%;\" onClick=\"GagneFocus('li');\" value=\"Style(s) pour toute la ligne\" readonly>
	<input type=\"hidden\" class=\"Saisie\" id=\"st-li\" name=\"st-li\" value=\"\">
	</td></tr></table>
	<div class=\"Zone-Visu\">
	<table class=\"Visu_Table\">";
	if ($nbrcol == 2) {
		$Form.="\n<tr><td><textarea id=\"ch-0\" name=\"ch-0\" rows=\"4\" style=\"font-size: 14px;width: 100%;border:solid 1px;background-color:#eeeeee;$Styles[0]\" onFocus=\"GagneFocus('0')\">".str_replace("<br>","\n",$tab[0])."</textarea>
		<input type=\"hidden\" id=\"st-0\" name=\"st-0\" value=\"$Styles[0]\"></td></tr>";
		$Form.="\n<tr><td><textarea id=\"ch-1\" name=\"ch-1\" rows=\"4\" style=\"font-size: 14px;width: 100%;border:solid 1px;background-color:#eeeeee;$Styles[1]\" onFocus=\"GagneFocus('1')\">".str_replace("<br>","\n",$tab[1])."</textarea>
		<input type=\"hidden\" id=\"st-1\" name=\"st-1\" value=\"$Styles[1]\"></td></tr>";
	} else {
		for ($i=0 ;$i<$nbrcol; $i++) {
			if ($tmp[3] == "O") {
				// si Entete
				$ColTitre=$TitreCol[$i];
			} else {
				$j=$i+1;
				$ColTitre="colonne ".$j;
			}
			$L1.="\n<td>$ColTitre</td>";
			$L2.="\n<td><input type=\"text\" class=\"Saisie\" id=\"ch-$i\" name=\"ch-$i\" style=\"min-width:100%;font-size: 15px;border:solid 1px;background-color:#eeeeee;$Styles[$i]\" value=\"$tab[$i]\" onFocus=\"GagneFocus('$i')\">
			<input type=\"hidden\" id=\"st-$i\" name=\"st-$i\" value=\"$Styles[$i]\"></td>";
		}	
	}
	$Form.="<tr>$L1</tr><tr>$L2</tr></table></div>
	<div style=\"width=100%;text-align:center;border:1px solid red;\"><table><tr>
	<td><input type=\"text\" class=\"Saisie\" id=\"Op1\"></td>
	<td><input type=\"button\" class=\"Option_N1\" id=\"Operation\" style=\"font-weight:bold;font-size:20px;\" onClick=\"DefileValeur('Operation','+;-;*;/');\" value=\"+\"></td>
	<td><input type=\"text\" class=\"Saisie\" id=\"Op2\"></td>
	<td><input type=\"button\" class=\"Option_N1\" id=\"Calcul\" style=\"font-weight:bold;font-size:20px;\" onClick=\"Calculer();\" value=\"=\"></td>
	<td><input type=\"text\" class=\"Saisie\" id=\"Result\"></td>
	</tr></table></div>
	<div style=\"width=100%;text-align:center;\">
	<img src=\"./Icones/Supprimer.png\" onclick=\"document.forms[0].QuoiVeux.value='ML;$ActionDmd[1];$ActionDmd[2];SL;$Idx';document.forms[0].submit();\" style=\"width:50px; height=50px;\">
	<img src=\"./Icones/PageScinder.png\" onclick=\"document.forms[0].QuoiVeux.value='PS;$ActionDmd[1];$ActionDmd[2];$Idx';document.forms[0].submit();\" style=\"width:50px; height=50px;\">
	<img src=\"./Icones/LignePlusAvant.png\" onclick=\"document.forms[0].QuoiVeux.value='ML;$ActionDmd[1];$ActionDmd[2];Av;$Idx';document.forms[0].submit();\" style=\"width:50px; height=50px;\">
	<img src=\"./Icones/LignePlusApres.png\" onclick=\"document.forms[0].QuoiVeux.value='ML;$ActionDmd[1];$ActionDmd[2];Ap;$Idx';document.forms[0].submit();\" style=\"width:50px; height=50px;\">
	<img src=\"./Icones/Valider.png\" onclick=\"document.forms[0].QuoiVeux.value='ML;$ActionDmd[1];$ActionDmd[2];ML;$Idx';document.forms[0].submit();\" style=\"width:50px; height=50px;\">
	<img src=\"./Icones/Annuler.png\" onclick=\"document.forms[0].QuoiVeux.value='Aff;$ActionDmd[1];$ActionDmd[2]';document.forms[0].submit();\" style=\"width:50px; height=50px;\">
	</div>
	</center></div>";
	Aff($ActionDmd);
}
// Demande de modification de page
function DMP (&$ActionDmd) {
	global $IdxPages,$ChoixStyle,$ChoixCoul,$Table,$Infos,$MsgErr,$Form;
	RecupFichiers($ActionDmd);
	$IdxDmd=$IdxPages[($ActionDmd[2]-1)];
	$InfosPage=$Infos[$IdxDmd];
	$tmp=explode(",",$InfosPage);
	$Form.="
	<div id=\"Zone-Ctl\" class=\"Zone-Visu\"><center>
	<div class=\"Titre\">Modification page :  $tmp[1]</div>";
	if ($MsgErr != "") {
		$Form.="\n<br><label style=\"color: red;font-weight: bold;\">$MsgErr</label><br>";
	}
	for ($i=0; $i<3; $i++) {
		$checked="checkgroup$i";
		$$checked="";
		if ($tmp[2]==$i) {
			$$checked="checked";
		}
	}
	if ($tmp[3] == "O") {
		$check="checked";
	} else {	
		$check="";
	}
	if ($tmp[7] == "") {$tmp[7]="red";}
	if ($tmp[8] == "") {$tmp[8]="oui";}
	$Form.="<div style=\"width:100%;border:1px solid red;overflow-x: scroll;\">
	$ChoixCoul<table style=\"border:solid 1px black;\"><tr>
	<td>Couleur bordures</td>
	<td><input type=\"text\" class=\"Saisie\" id=\"CoulBord\" name =\"CoulBord\" onClick=\"DefileValeur('CoulBord','black;blue;green;red;aucune');\" value=\"$tmp[7]\" style=\"width:70px;text-align:center;\" readonly></td>
	<td>Couleur de fond</td>
	<td><input type=\"text\" class=\"Saisie\" id=\"CoulFond\" name=\"CoulFond\" onClick=\"GagneFocus('CoulFond');\" value=\"$tmp[6]\" style=\"width:70px;text-align:center;\" readonly></td>
	<td>Etendre</td>
	<td><input type=\"text\" class=\"Saisie\" id=\"Etendre\" name =\"Etendre\" onClick=\"DefileValeur('Etendre','oui;non');\" value=\"$tmp[8]\" style=\"width:60px;text-align:center;\" readonly></td>
	</tr></table></div>
	$ChoixStyle
	<div style=\"width:100%;border:1px solid red;overflow-x: scroll;\">
	<table><tr><td rowspan=\"2\">
	<img src=\"./Icones/Supprimer.png\" onclick=\"document.forms[0].QuoiVeux.value='SP;$ActionDmd[1];$ActionDmd[2]';document.forms[0].submit();\" style=\"width:50px; height=50px;\"></td>
	<td><label style=\"font-weight: bold;\">Titre</label>
	<input type=\"text\" class=\"Saisie\" id=\"ch-Titre\" name=\"Titre\" value=\"$tmp[1]\" style=\"width:130px;$tmp[5]\" onFocus=\"GagneFocus('Titre');\">
	<input type=\"hidden\" class=\"Saisie\" id=\"st-Titre\" name=\"Style\" value=\"$tmp[5]\">
	<label style=\"font-weight: bold;\">Entete colonnes 
	<input type=\"checkbox\" id=\"Entete\" name=\"Entete\" $check></label></td>
	<td rowspan=\"2\">
	<img src=\"./Icones/Valider.png\" onclick=\"document.forms[0].QuoiVeux.value='MP;$ActionDmd[1];$ActionDmd[2]';document.forms[0].submit();\" style=\"width:50px; height=50px;\">
	<img src=\"./Icones/Annuler.png\" onclick=\"document.forms[0].QuoiVeux.value='Aff;$ActionDmd[1];$ActionDmd[2]';document.forms[0].submit();\" style=\"width:50px; height=50px;\">
	<img src=\"./Icones/Imprimer.png\" onclick=\"Imprimer('Page-$ActionDmd[2]');\" style=\"width:50px; height=50px;\">
	</td></tr><tr>
	<td><label style=\"font-weight: bold;\">Grouper</label>
	<label style=\"font-weight: bold;\">
	<input type=\"radio\" id=\"checkgroup1 \" name=\"Grouper\" value=\"0\" $checkgroup0> Non</label>
	<label style=\"font-weight: bold;\">
	<input type=\"radio\" id=\"checkgroup2 \" name=\"Grouper\" value=\"1\" $checkgroup1> Non décalé</label>
	<label style=\"font-weight: bold;\">
	<input type=\"radio\" id=\"checkgroup3 \" name=\"Grouper\" value=\"2\" $checkgroup2> Décalé</label></td>
	</tr></table></div>
	<div class=\"Zone-Visu\">
	<table class=\"Visu_Table\"><tr>\n";
	$tot=explode("-",$tmp[4]);
	// pour les colonnes
	$TitreCol=explode(";",$Table[$IdxDmd+1]);
	for($i=0; $i<count($tot); $i ++) {
		if ($tmp[3] == "O") {
			// si Entete
			$ColTitre=$TitreCol[$i];
		} else {
			$j=$i+1;
			$ColTitre="colonne ".$j;
		}
		if ($i == 0) {
			$L1.="\n<td rowspan=\"2\"><img id=\"InvLigneCol\" name=\"InvLigneCol\" src=\"./Icones/LigneEnColonne.png\" onclick=\"document.forms[0].QuoiVeux.value='ILC;$ActionDmd[1];$ActionDmd[2]';document.forms[0].submit();\" style=\"width:45px; height=45px;\"></td>
			<td rowspan=\"2\"><img id=\"ColDecalAv\" name=\"ColDecalAv\" src=\"./Icones/PageDecalAvant.png\" onclick=\"document.forms[0].QuoiVeux.value='DC;$ActionDmd[1];$ActionDmd[2];Av;$i';document.forms[0].submit();\" style=\"width:40px; height=40px;\"></td>
			<td><input type=\"text\" class=\"Saisie\" id=\"ch-$i\" name=\"ch-$i\" value=\"$ColTitre\" style=\"$tmp[5]\" readonly onclick=\"GagneFocus('$i');\"></td>
			<td rowspan=\"2\"><img id=\"ColDecalAp\" name=\"ColDecalAp\" src=\"./Icones/PageDecalApres.png\" onclick=\"document. forms[0].QuoiVeux.value='DC;$ActionDmd[1];$ActionDmd[2];Ap;$i';document.forms[0].submit();\" style=\"width:40px; height=40px;\">
			<input type=\"hidden\" class=\"Saisie\" id=\"st-$i\" name=\"st-$i\" value=\"\"></td>";
		} else {
			$L1.="\n<td><input type=\"text\" class=\"Saisie\" id=\"ch-$i\" name=\"ch-$i\" value=\"$ColTitre\" style=\"$tmp[5]\" readonly onclick=\"GagneFocus('$i');\">
			<td rowspan=\"2\"><img id=\"ColDecalAp\" name=\"ColDecalAp\" src=\"./Icones/PageDecalApres.png\" onclick=\"document. forms[0].QuoiVeux.value='DC;$ActionDmd[1];$ActionDmd[2];Ap;$i';document.forms[0].submit();\" style=\"width:40px; height=40px;\">
			<input type=\"hidden\" class=\"Saisie\" id=\"st-$i\" name=\"st-$i\" value=\"\"></td>";
		}
		$L2.="\n<td style=\"text-align:center;vertical-align:top;\">
		<img src=\"./Icones/Supprimer.png\" style=\"width:25px; height=25px;\">
		<input type=\"checkbox\" id=\"mc-$i\" name=\"mc-$i\" value=\"M\" style=\"width:25px;height:25px;\"> 
		<input style=\"width:25px;height:25px;vertical-align:top;font-weight:bold;text-align:center;background-color:#eeeeee;\" type=\"text\" id=\"tc-$i\" name=\"tc-$i\" value=\"$tot[$i]\" readonly onClick= \"MajTotal($i)\">&nbsp
		<input type=\"checkbox\" id=\"pc-$i\" name=\"pc-$i\" value =\"P\" style=\"width:25px;height:25px;\">
		<img src=\"./Icones/Ajouter.png\" style=\"width:25px; height=25px;\">";
	}
	$Form.="\n$L1</tr><tr>$L2</tr></table></div>
	</center></div>";
	Aff($ActionDmd);
}
// Demande d'insertion de page
function DIP (&$ActionDmd) {
	global $MsgErr,$Form;
	$ou="Avant";
	if ($ActionDmd[3] == "Ap") {
		$ou="Après";
	}
	$Form.="
	<div id=\"Zone-Ctl\" class=\"Zone-Visu\"><center>
	<div class=\"Titre\">Insertion d'une page $ou</div>";
	if ($MsgErr != "") {
		$Form.="\n<br><label style=\"color: red;font-weight: bold;\">$MsgErr</label><br>";
	}
	$Form.="
	<fieldset>
	<legend style=\"align: top;color: blue;font-weight: bold;\" >Création ex nihilo</legend>
	<table><tr>
	<td><label style=\"font-weight: bold;\">Titre</label>
	<input type=\"text\" class=\"Saisie\" name=\"Titre\" style=\"width:250px;\"></td>
	<td rowspan=\"2\"><img src=\"./Icones/Valider.png\" onclick=\"document.forms[0].QuoiVeux.value='IP;$ActionDmd[1];$ActionDmd[2];AN;$ActionDmd[3]';document.forms[0].submit();\" style=\"width:50px; height=50px;\"></td></tr>";
	if ($ActionDmd[1] == "Menu-Main") {
		$Form.="</table></fieldset>";
	} else {
		$Form.="<tr><td><label style=\"font-weight: bold;\">Nombre de colonnes (par onglet) </label>
		<input type=\"text\" class=\"Saisie\" name=\"NbrCol\" value=\"5\" style=\"width:25px;text-align:center;\"></td></tr>
		</table></fieldset>
		<fieldset>
		<legend style=\"align: top;color: blue;font-weight: bold;\" >Création par import</legend>
		<table><tr>
		<td><label style=\"font-weight: bold;\">Selectionner le fichier </label>
		<input type=\"file\" class=\"Saisie\" name=\"import\" accept=\".csv\" style=\"width:250px;\"></td></tr>
		<tr><td><label style=\"font-weight: bold;\">Preciser le séparateur de colonne </label>
		<input type=\"text\" class=\"Saisie\" name=\"Separateur\" value=\";\" style=\"width:25px;text-align:center;\"></td>
		<td rowspan=\"2\"><img src=\"./Icones/Valider.png\" onclick=\"document.forms[0].QuoiVeux.value='IP;$ActionDmd[1];$ActionDmd[2];PI;$ActionDmd[3]';document.forms[0].submit();\" style=\"width:50px; height=50px;\"></td></tr>
		</table></fieldset>";
	}
	$Form.="<img src=\"./Icones/Annuler.png\" onclick=\"document.getElementById('Zone-Ctl').style.display='none';\" style=\"width:50px; height=50px;\">
	</center></div>";
	Aff($ActionDmd);
}
// Demande de modification de fichier
function DMF (&$ActionDmd) {
	global $Infos,$MsgErr,$Form,$ChoixStyle;
	RecupFichiers($ActionDmd);
	$ReadOnly="";
	if ($ActionDmd[1] == "Menu-Main") {
		$ReadOnly="readonly";
	}
	$Form.="
	<div id=\"Zone-Ctl\" class=\"Zone-Visu\"><center>
	<div class=\"Titre\">Modification du fichier : $ActionDmd[1]</div>";
	if ($MsgErr != "") {
		$Form.="\n<br><label style=\"color: red;font-weight: bold;\">$MsgErr</label><br>";
	}
	$tmp=explode(",",$Infos[0]);
	$Form.="$ChoixStyle
	<br><label style=\"font-weight: bold;\">Intitulé pour le menu</label>
	<input type=\"text\" class=\"Saisie\" name=\"Nom\" value=\"$ActionDmd[1]\" style=\"width:150px;\" $ReadOnly>
	<br><br><label style=\"font-weight: bold;\">Titre</label>
	<input type=\"text\" class=\"Saisie\" id=\"ch-Titre\" name=\"Titre\" value=\"$tmp[0]\" style=\"width:250px;$tmp[1];\" $ReadOnly>
	<input type=\"hidden\" class=\"Saisie\" id=\"st-Titre\" name=\"Style\" value=\"$tmp[1]\">
	<br><br>
	<table><tr>";
	if ($ActionDmd[1] != "Menu-Main") {
		$Form.="<td><img src=\"./Icones/Supprimer.png\" onclick=\"document.forms[0].QuoiVeux.value='SF;$ActionDmd[1]';document.forms[0].submit();\" style=\"width:40px; height=40px;\"></td><td>&nbsp</td>";
	}
	$Form.="<td><img src=\"./Icones/Sauvegarder.png\" onclick=\"document.forms[0].QuoiVeux.value='SD;$ActionDmd[1]';document.forms[0].submit();\" style=\"width:35px; height=35px;\"></td>
	<td><img src=\"./Icones/Restaurer.png\" onclick=\"document.forms[0].QuoiVeux.value='RD;$ActionDmd[1]';document.forms[0].submit();\" style=\"width:35px; height=35px;\"></td>
	<td><img src=\"./Icones/Valider.png\" onclick=\"document.forms[0].QuoiVeux.value='MF;$ActionDmd[1]';document.forms[0].submit();\" style=\"width:50px; height=50px;\"></td>
	<td><img src=\"./Icones/Annuler.png\" onclick=\"document.forms[0].QuoiVeux.value='Aff;$ActionDmd[1];0';document.forms[0].submit();\" style=\"width:50px; height=50px;\"></td>
	</tr></table>
	</center></div>";
	Aff($ActionDmd);
}
// Demande de création de fichier
function DCF (&$ActionDmd) {
	global $MsgErr,$Form,$Menu;
	$Form.="
	<div id=\"Zone-Ctl\" class=\"Zone-Visu\"><center>
	<div class=\"Titre\">Creation d'un nouveau Classeur</div>";
	if ($MsgErr != "") {
		$Form.="\n<br><label style=\"color: red;font-weight: bold;\">$MsgErr</label><br>";
	}
	$Form.="
	<fieldset>
	<legend style=\"align: top;color: blue;font-weight: bold;\" >Création ex nihilo</legend>
	<img src=\"./Icones/Valider.png\" onclick=\"document.forms[0].QuoiVeux.value='CF;Nouveau';document.forms[0].submit();\" style=\"width:50px; height=50px;\">
	<br><label style=\"font-weight: bold;\">Intitulé pour le menu</label>
	<input type=\"text\" class=\"Saisie\" name=\"Nom\" style=\"width:150px;\">
	<br><br><label style=\"font-weight: bold;\">Titre</label>
	<input type=\"text\" class=\"Saisie\" name=\"Titre\" style=\"width:250px;\">
	<br><br><label style=\"font-weight: bold;\">Nombre d'onglets </label>
	<input type=\"text\" class=\"Saisie\" name=\"NbrPage\" value=\"1\" style=\"width:25px;text-align:center;\">
	<label style=\"font-weight: bold;\">Nombre de colonnes (par onglet) </label>
	<input type=\"text\" class=\"Saisie\" name=\"NbrCol\" value=\"5\" style=\"width:25px;text-align:center;\">
	</fieldset><fieldset>
	<legend style=\"align: top;color: blue;font-weight: bold;\" >Création par import</legend>
	<img src=\"./Icones/Valider.png\" onclick=\"document.forms[0].QuoiVeux.value='CF;Import';document.forms[0].submit();\" style=\"width:50px; height=50px;\">
	<br><label style=\"font-weight: bold;\">Selectionner le fichier </label>
	<input type=\"file\" class=\"Saisie\" name=\"import\" accept=\".csv\" style=\"width:300px;\">
	<br><br><label style=\"font-weight: bold;\">Preciser le séparateur de colonne </label>
	<input type=\"text\" class=\"Saisie\" name=\"Separateur\" value=\";\" style=\"width:25px;text-align:center;\">
	</fieldset>
	<img src=\"./Icones/Annuler.png\" onclick=\"Aff_Menu(0,'Changer;$Menu');\" style=\"width:50px; height=50px;\"></td>
	</center></div>";
}
?>