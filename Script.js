var mediaQueryList = window.matchMedia("(orientation: portrait)");
function ChangeOrientation() {
// en vrai écran : 800 x 1280
	if (mediaQueryList.matches) {
		// portrait 980 * 1280
		document.forms[0].Oriente.value="portrait";
	} else {
		// paysage 980 * 433
		document.forms[0].Oriente.value="paysage";
	}
//	document.forms[0].submit();
}
mediaQueryList.addListener(ChangeOrientation);
// variables globales
var ColMod=-1;
var hauteur=0;
var ModeAff=2;
// couleur pour les boutons
var FondVert="#66cc66";	
var FondBleu="#ccccff";	
var FondOrange="#ff9933";
// gestion pages
var PageEncours;
function InitFrame() {
// en vrai écran : 800 x 1280 (980*1280)
// var hauteur = document.body.offsetHeight;
// var ratio = window.devicePixelRatio; // 1,33
//	var largeur = window.innerWidth;
	hauteur = window.innerHeight;
	var tete=document.getElementById("entete");
	var he=tete.offsetHeight;
	var nav=document.getElementById("AffMnu-0");
	var hm=nav.offsetHeight;
	var ht=0;
	var tfich=document.getElementById("TitreFichier");
	if (tfich != null) {
		ht=tfich.offsetHeight;
	}
	var hc=0;
	var zctl=document.getElementById("Zone-Ctl");
	if (zctl != null) {
		hc=zctl.offsetHeight;
	}
	var hp=0;
	var pctl=document.getElementById("img-0");
	if (pctl != null) {
		hp=pctl.offsetHeight;
	}
	hauteur=hauteur-he-hm-ht-hc-hp;
	document.forms[0].Haff.value=hauteur;
	var visu=document.getElementById("zone-visu");
	if (visu != null) {
		visu.style.height=hauteur+"px";
	}
//600*692	
}
// maximiser minimiser la fenêtre 
function MinMaxFen() {
//	var elem=document.getElementById("MinMaxFen");
	if (document.mozFullScreenEnabled) {
		if (!document.mozFullScreenElement) {
			document.documentElement.mozRequestFullScreen();
		} else {
//			document.mozCancelFullScreen();
		}
	}
/*	if (document.fullscreenElement) {
	  if (!document.fullscreenElement) {
	    document.requestFullscreen();
	  } else {
	    document.exitFullscreen();
	  }
	}*/
}
// pour changer d utilisateur
function Change_Qui() {
	document.forms[0].Qui.value='';
	document.forms[0].submit();
}
// pour l'affichage des menus
function Aff_Menu(Num,Action) {
	// Action = Href N2 Autre;param
	var Quoi=Action.split(';');
	if (Quoi[0] == "Href") {
		window.open(Quoi[1]);
		Quoi[0]="N2";
	}
	if (Quoi[0] == "N2") {
		// flipper flop si rappel
		var aff = "AffMnu-" + Num;
		var ya=document.getElementById(aff);
		if (ya.style.display == "block") {
		ya.style.display="none";
		return;
		}
		// afficher masquer les sous menus
		var elem=document.getElementsByName("AffMnu");
		var opts=document.getElementsByClassName("Option_N1");
		var nbr_elem=elem.length;
		for (var i=1; i<nbr_elem; i++) {
			j=i-1;
			elem[i].style.display="none";
			opts[j].style.backgroundColor=FondBleu;
			if (elem[i].id == aff)	{
				elem[i].style.display="block";
				opts[j].style.backgroundColor=FondVert;
			}
		}
	} else {
		// changer de menu ou aff
		document.forms[0].QuoiVeux.value=Quoi[0]+";"+Quoi[2]+";1";
		document.forms[0].Menu.value=Quoi[1];
		document.forms[0].submit();
	}
}
// pour afficher masquer l'aide
function AffAide(quoi) {
	var Aide="Aide-"+quoi;
	var elem=document.getElementById(Aide);
	if (elem.style.display == "none") {
		elem.style.display="block";
	} else {
		elem.style.display="none";
	}
	InitFrame();
}
// pour affichage par page(s)
function Aff_Page (Num) {
	// annulation dmd modif page
	var zctl=document.getElementById("Zone-Ctl");
	if (zctl != null) {
		zctl.style.display = "none";
	}
	// afficher masquer les pages
	var page = "Page-" + Num;
	if (ModeAff == 2){
		// voir 1 page
		var elem=document.getElementsByName("Page");
		var nbr_elem=elem.length;
		var j=0;
		for (var i=0; i<=nbr_elem; i++) {
			j=i+1;
			var btn = "BtnPage-" + j;
			var ctl=document.getElementById(btn);
			ctl.style.backgroundColor=FondBleu;
			elem[i].style.display="none";
			if (elem[i].id == page)	{
				elem[i].style.display="block";
				ctl.style.backgroundColor=FondVert;
			}
		}
	} else {
		// voir plusieures pages
		var btn = "BtnPage-" + Num;
		var elem=document.getElementById(page);
		var ctl=document.getElementById(btn);
		if (elem.style.display == "none") {
			elem.style.display="block";
			ctl.style.backgroundColor=FondVert;
		} else {
			elem.style.display="none";
			ctl.style.backgroundColor=FondBleu;
		}	
	}
	InitFrame();
}
// flip flop mode continu mode page
function Mode_Aff (Mode) {
	// annulation dmd modif page
	var zctl=document.getElementById("Zone-Ctl");
	if (zctl != null) {
		zctl.style.display = "none";
	}
	ModeAff=Mode;
	var VoirImg = [
	['none','none','none','none','none','block','block','none','none','none'], 			// une seule page
	['none','block','none','none','none','none','none','none','none','none'], 			// de pages(s) à continu
	['block','none','block','block','none','block','block','block','block','none'],		// de pages à une seule
	['block','none','block','none','block','none','none','none','none','block']			// de 1 pages à plusieurs
	];
	for (var i=0; i<10; i++)	{
		var img="img-"+i;
		img=document.getElementById(img);
		img.style.display=VoirImg[Mode][i];
	}
	var pages=document.getElementsByName("Page");
	var display = "none";
	if (Mode == 1) {display = "block";}
	var Fond = FondVert;
	for (var i=0; i<pages.length; i++) {
		var j=i+1;
		var btn = "BtnPage-" + j;
		var ctl=document.getElementById(btn);
		ctl.style.backgroundColor=Fond;
		pages[i].style.display=display;
		Fond = FondBleu;
	}
	pages[0].style.display="block";
	InitFrame();
}
// pour inserer 1 page ou fusionner ou décaler
function Quoi_Veux(Quoi) {
	var tmp=Quoi.split(";");
	var pages=document.getElementsByName("Page");
	if (tmp[0] == "FP") {
		// Fusion
		var Psel=new Array();
		var j=0;
		// on récupère les pages sélectionnées
		for (var i=0; i<pages.length; i++) {
			if (pages[i].style.display == "block") {
				var Pnum=pages[i].id.split('-');
				Psel[j]=parseInt(Pnum[1]);
				j++;
			}
		}
		// on extrait les pages contiguës
		var num=Psel[0];
		for (var i=1; i<Psel.length; i++) {
			var att=Psel[i-1] + 1;
			if (Psel[i] == att) {
				num=num+"-"+Psel[i];
			} else {break;}
		}
		if (num.length >= 3) {
			var QuoiVeux=tmp[0]+";"+tmp[1]+";"+num;
			document.forms[0].QuoiVeux.value=QuoiVeux;
			document.forms[0].submit();
		}
	} else {
		// Insertion ou Decallage
		for (var i=0; i<pages.length; i++) {
			if (pages[i].style.display == "block") {
				var Pnum=pages[i].id.split('-');
				break;
			}
		}
		var QuoiVeux=tmp[0]+";"+tmp[1]+";"+Pnum[1]+";"+tmp[2];
		document.forms[0].QuoiVeux.value=QuoiVeux;
		document.forms[0].submit();
	}
}
// pour le menu Afficher
function Afficher(Quoi) {
	var Action=Quoi.split(';');
	if (Action[0] == "Href") {
		window.open(Action[1]);
		Aff_Menu(0,'N2');	
	}else{
		hauteur = window.innerHeight;
		var tete=document.getElementById("entete");
		var ht=tete.offsetHeight;
		var nav=document.getElementById("Mnu-0");
		var hn=nav.offsetHeight;
		var h=15;
		hauteur=hauteur-ht-hn-h;
		document.forms[0].Haff.value=hauteur;
		var QuoiVeux="Aff;Rien";
		document.forms[0].Aff.value=QuoiVeux;
		document.forms[0].submit();
	}
}
// pour les modifs de ligne
function GagneFocus(col) {
	ColMod=col;
	ColVoir="ch-"+ColMod;
	var elem=document.getElementsByClassName("Saisie");
	var nbr_elem=elem.length;
	for (var i=0; i<nbr_elem; i++) {
		elem[i].style.backgroundColor="#eeeeee";
		elem[i].style.width="100%";
	}
	var Voir=document.getElementById(ColVoir);
	Voir.style.backgroundColor="#f0e68c";
	if (col != "li") {
		Voir.style.width="200px";
	}
}
// pour l'alignement
function MajAligne(ou) {
	// text-align : left/center/normal
	if (ColMod == -1) {return;}
	// pour la visu
	var ColVoir="ch-"+ColMod;
	var Voir=document.getElementById(ColVoir);
	var ColInfo="st-"+ColMod;
	var Info=document.getElementById(ColInfo);
	// pour la modif
	var nouveau="text-align:"+ou+";";
	var tmp=Info.value.split(";");
	var nbr=tmp.length-1;
	var yavait="N";
	var Enreg="";
	if (tmp.length == 1) {
		yavait="O";
		Enreg=nouveau;
	} else{
		for (var i=0; i<nbr; i++) {
			if (tmp[i].indexOf('text-align') == 0) {
				yavait="O";
				if (ou != "left") {
					Enreg=Enreg+nouveau;
				}
			} else {
				Enreg=Enreg+tmp[i]+";";
			}
		}
	}
	if (yavait == "N" && ou != "black") {
		Enreg=Enreg+nouveau;
	}
	Info.value=Enreg;
	Voir.style.textAlign=ou;
	Voir.focus();
}
// pour la couleur
function MajCouleur(couleur) {
	// color : black/red/blue/green
	if (ColMod == -1) {return;}
	// pour la visu
	var ColVoir="ch-"+ColMod;
	var Voir=document.getElementById(ColVoir);
	var ColInfo="st-"+ColMod;
	var Info=document.getElementById(ColInfo);
	// pour la modif
	var nouveau="color:"+couleur+";";
	var tmp=Info.value.split(";");
	var nbr=tmp.length-1;
	var yavait="N";
	var Enreg="";
	if (tmp.length == 1) {
		yavait = "O";
		Enreg=nouveau;
	} else{
		for (var i=0; i<nbr; i++) {
			if (tmp[i].indexOf('color') == 0) {
				yavait="O";
				if (couleur != "black") {
					Enreg=Enreg+nouveau;
				}
			} else {
				Enreg=Enreg+tmp[i]+";";
			}
		}
	}
	if (yavait == "N" && couleur != "black") {
		Enreg=Enreg+nouveau;
	}
	Info.value=Enreg;
	Voir.style.color=couleur;
	Voir.focus();
}
// pour la graisse
function MajGras() {
	// font-weight : normal/bold
	if (ColMod == -1) {return;}
	// pour la visu
	var ColVoir="ch-"+ColMod;
	var Voir=document.getElementById(ColVoir);
	var ColInfo="st-"+ColMod;
	var Info=document.getElementById(ColInfo);
	var graisse=Voir.style.fontWeight;
	if (graisse == "bold") {
		graisse="normal";
		var Enreg=Info.value.replace("font-weight:bold;","");
	} else {
		graisse="bold";
		var Enreg=Info.value+"font-weight:bold;";
	}
	Info.value=Enreg;
	Voir.style.fontWeight=graisse;
	Voir.focus();
}
// pour le style
function MajStyle() {
	// font-style : normal/italic
	if (ColMod == -1) {return;}
	// pour la visu
	var ColVoir="ch-"+ColMod;
	var Voir=document.getElementById(ColVoir);
	var ColInfo="st-"+ColMod;
	var Info=document.getElementById(ColInfo);
	var Style=Voir.style.fontStyle;
	if (Style == "italic") {
		Style="normal";
		var Enreg=Info.value.replace("font-style:italic;","");
	} else {
		Style="italic";
		var Enreg=Info.value+"font-style:italic;";
	}
	Info.value=Enreg;
	Voir.style.fontStyle=Style;
	Voir.focus();
}
// pour la deco
function MajDeco() {
	// text-decoration : normal/underline
	if (ColMod == -1) {return;}
	// pour la visu
	var ColVoir="ch-"+ColMod;
	var Voir=document.getElementById(ColVoir);
	var ColInfo="st-"+ColMod;
	var Info=document.getElementById(ColInfo);
	var Deco=Voir.style.textDecoration;
	if (Deco == "underline") {
		Deco="none";
		var Enreg=Info.value.replace("text-decoration:underline;","");
	} else {
		Deco="underline";
		Enreg=Info.value+"text-decoration:underline;";
	}
	Info.value=Enreg;
	Voir.style.textDecoration=Deco;
	Voir.focus();
}
// pour les totaux
function MajTotal(col) {
	var ColId="tc-"+col;
	var elem=document.getElementById(ColId);
	if (elem.value == "n") {
		elem.value='\u2211';
	} else {
		elem.value="n";
	}
}
// pour la couleur de fond
function MajFond(Couleur) {
	var elem=document.getElementById(ColMod);
	elem.style.backgroundColor=Couleur;
	document.forms[0].CoulFond.value=Couleur;
}
// scrolling des valeurs
function DefileValeur(Ctl,Val) {
	var elem=document.getElementById(Ctl);
	var Valeurs=Val.split(";");
	var NewVal=0;
	for (var i=0; i<Valeurs.length; i++) {
		NewVal=i+1;
		if (NewVal == Valeurs.length) {NewVal=0;}
		if (elem.value == Valeurs[i]) {
			elem.value=Valeurs[NewVal];
			break;	
		}
	}
}
// petit calcul
function Calculer() {
	var Oper1=document.getElementById('Op1');
	var Oper2=document.getElementById('Op2');
	var Operation=document.getElementById('Operation');
	var Result=document.getElementById('Result');
	var Faire=Oper1.value+Operation.value+Oper2.value;
	Result.value=eval(Faire);
}
// imprimer la page
function Imprimer(objet) {
	var zone = document.getElementById(objet).innerHTML;
	var fen = window.open("", "", "toolbar=0, menubar=0, scrollbars=1, resizable=1,status=0, location=0, left=10, top=10");
	fen.document.body.innerHTML += " " + zone + " ";
	fen.window.print();
	fen.window.close();
}
