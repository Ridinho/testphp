<?php
$StyleDef="";
$Table=array();
$Infos=array();
$IdxPages=array();
$NbrLignes=0;
$MsgErr="";
function RecupFichiers (&$ActionDmd) {
	global $Repert,$Table,$Infos,$StyleDef,$IdxPages,$NbrLignes;
	// raz (pour les imports)
	$Table=array();
	$Infos=array();
	$IdxPages=array();
	// on récupère les fichiers	
	$FichierCsv="$Repert".$ActionDmd[1].".csv";
	$FichierInf="$Repert".$ActionDmd[1].".inf";
	if (file_exists($FichierCsv)) {
		// les 2 existent
		$NbrLignes=0;
		$handle = fopen($FichierCsv, "r");
		while (!feof($handle)) {
		   	$tmp = fgets($handle, 4096);
			if($tmp === false) break;
  		   	$tmp = trim($tmp);
		   	$Table[$NbrLignes] = $tmp;
		   	$NbrLignes++;
		}
		fclose($handle);
		if (file_exists($FichierInf)) {
			$NbrInfos=0;
			$i=0;
			$handle = fopen($FichierInf, "r");
			while (!feof($handle)) {
			   	$tmp = fgets($handle, 4096);
		    	if($tmp === false) break;
		    	$tmp=trim($tmp);
				$ya=strpos($tmp,'NP');
				if ($ya===0) {
		    		$IdxPages[$i]=$NbrInfos;
		    		$i++;
				}
	 			$Infos[$NbrInfos]=$tmp;
 				$NbrInfos++;
		 	}
			fclose($handle);
		} else {
			// le fichier est absent on traite en import
			$sep=";";
			if (isset($_POST['Separateur'])) {
				$sep=$_POST['Separateur'];
			}
			// pour récupérer le nbr max de col
			$MaxCol=0;
			for ($i=0; $i<$NbrLignes; $i++) {
				$tmp=explode($sep,$Table[$i]);
				$NbrCol=count($tmp);
				if ($NbrCol > $MaxCol) {$MaxCol=$NbrCol;}
			}
			// pour inserer titre et page 1
			$NbrPages=1;
			array_splice($Table,0,0,$ActionDmd[1]);
			array_splice($Table,1,0,"Page 1");
			$Totaux="n";
			for ($j=1; $j<$MaxCol; $j++) {
				$Totaux .= "-n";
			}
			$NewPage="NP,Import ".$NbrPage.",0,N,".$Totaux;
			$Infos[0]=$ActionDmd[1];
			$Infos[1]=$NewPage;
			$NbrLignes +=2;
			$IdxPages[0]=1;
			// on recree le fichier correctement
			for ($i=2; $i<$NbrLignes; $i++) {
				$tmp=explode($sep,$Table[$i]);
				$nbr=count($tmp);
				$lgr=strlen($Table[$i]);
				if ($lgr == ($nbr-1)) {
					// on cree une page sur ligne vide
					$NewPage="NP,Import $NbrPage,0,N,$Totaux";
					$Table[$i]="Import $NbrPages";
					$Infos[$i]=$NewPage;
					$IdxPages[]=$i;
				} else {
					$ligne=$tmp[0];
					$Styles=$StyleDef;
					for ($j=1; $j<$nbr; $j++) {
						$ligne.=";".$tmp[$j];
						$Styles.=",".$StyleDef;
					}
					for ($j=$nbr; $j<$MaxCol; $j++) {
						$ligne.=";";
						$Styles.=",".$StyleDef;
					}
					$Table[$i]=$ligne;
					$Infos[$i]=$Styles;
				}
			}
			EnregFichiers($ActionDmd);
		}
	} else {
		CF($ActionDmd);
	}	
}
// Enregistrer les fichiers
function EnregFichiers (&$ActionDmd) {
	global $Repert,$Table,$Infos,$IdxPages;
	$NbrLignes=count($Table);
	$FichierCsv="$Repert".$ActionDmd[1].".csv";
	$FichierInf="$Repert".$ActionDmd[1].".inf";
	$h1 = fopen($FichierCsv, "w");
	$h2 = fopen($FichierInf, "w");
	for ($i=0; $i<$NbrLignes; $i++)	{
		if ($i != $NbrLignes) {
			$tmp1=$Table[$i]."\n";
			$tmp2=$Infos[$i]."\n";
		}
		fwrite($h1,$tmp1);
		fwrite($h2,$tmp2);
	}
	fclose($h1);
	fclose($h2);
}
// Création nouvelle page
function CreePages($NbrPages,$NbrCol,$Titre,$ou) {
	global $StyleDef,$NbrLignes,$Table,$Infos,$IdxPages;
	$Style=$StyleDef;
	$TotPages=count($IdxPages);
	$Entete="colonne 1";
	$Valeur.="valeur 1";
	$Totaux="n";
	for ($i=1;$i<$NbrCol;$i++) {
		$num=$i+1;
		$Totaux.="-n";
		$Style.=",$StyleDef";
		$Entete.=";colonne $num";
		$Valeur.=";valeur $num";
	}
	$j=0;
	$k=1;
	$l=2;	
	for ($i=1;$i<=$NbrPages;$i++) {
		$Intitule="$Titre $i";
		if ($NbrPages == 1) {
			$Intitule=$Titre;
		}
		$NewInfos[$j]="NP,$Intitule,N,O,$Totaux,,,,";
		$NewInfos[$k]=$Style;
		$NewInfos[$l]=$Style;
		$NewTable[$j]=$Intitule;
		$NewTable[$k]=$Entete;
		$NewTable[$l]=$Valeur;
		$j+=3;
		$k+=3;
		$l+=3;
	}
	array_splice($Table,$ou,0,$NewTable);
	array_splice($Infos,$ou,0,$NewInfos);
}
// Modifier une ligne
function ML(&$ActionDmd) {
	global $Table,$Infos,$IdxPages;
	RecupFichiers($ActionDmd);
	$ou=$ActionDmd[2]-1;
	$IdxPage=$IdxPages[$ou];
	$tmp=explode(",",$Infos[$IdxPage]);
	$tot=explode("-",$tmp[4]);
	$IdxMod=$ActionDmd[4];
	// la nouvelle ligne
	for ($i=0; $i<count($tot); $i++) {
		if ($i > 0) {
			$NewValeur.=";";
			$NewInfos.=",";
		}
		$colst="st-".$i;
		if ($_POST['st-li'] != "") {
			$colst="st-li";
		}
		$colch="ch-".$i;
		$NewValeur.=str_replace("\n"," ",$_POST[$colch]);
		$NewInfos.=$_POST[$colst];
	}		
	// ou on la met ?
	if ($ActionDmd[3] == "ML") {
		$Table[$IdxMod]=$NewValeur;
		$Infos[$IdxMod]=$NewInfos;
	} elseif ($ActionDmd[3] == "SL") {
		array_splice($Infos,$IdxMod,1);
		array_splice($Table,$IdxMod,1);
	} elseif ($ActionDmd[3] == "Av") {
		array_splice($Infos,$IdxMod,0,$NewInfos);
		array_splice($Table,$IdxMod,0,$NewValeur);
	} else {
		array_splice($Infos,($IdxMod+1),0,$NewInfos);
		array_splice($Table,($IdxMod+1),0,$NewValeur);
	}
	EnregFichiers($ActionDmd);
	$ActionDmd[3]="";
	Aff($ActionDmd);
}
// Scinder une page
function PS(&$ActionDmd) {
	global $Table,$Infos,$IdxPages;
	RecupFichiers($ActionDmd);
	$ou=$ActionDmd[2]-1;
	$IdxPage=$IdxPages[$ou];
	$EnteteInfo=$Infos[$IdxPage];
	$EnteteValeur=$Table[$IdxPage];
	array_splice($Infos,$ActionDmd[3],0,$EnteteInfo);
	array_splice($Table,$ActionDmd[3],0,$EnteteValeur);
	EnregFichiers($ActionDmd);
	$ActionDmd[3]="";
	Aff($ActionDmd);
}
// Fusionner  des pages
function FP(&$ActionDmd) {
	global $Table,$Infos,$IdxPages;
	RecupFichiers($ActionDmd);
	$Fusion=explode("-",$ActionDmd[2]);
	// nombre max de colonnes ?
	$MaxCols=0;
	for ($i=0; $i<count($Fusion); $i++) {
		$ou=$Fusion[$i]-1;
		$Idx=$IdxPages[$ou];
		$NbrCols=count(explode(";",$Table[$Idx+1]));
		if ($NbrCols > $MaxCols) {
			$EnteteInf=$Infos[$Idx];
		}
	}
	$ou=$Fusion[0]-1;
	$Idx=$IdxPages[$ou];
	array_splice($Infos,$Idx,0,$Entete);
	for ($i=1; $i<count($Fusion); $i++) {
		$ou=$Fusion[$i]-1;
		$Idx=$IdxPages[$ou];
		array_splice($Table,$Idx,1);
		array_splice($Infos,$Idx,1);
	}
	EnregFichiers($ActionDmd);
	$ActionDmd[2]=$Fusion[0];
	Aff($ActionDmd);
}
// Supprimer une page
function SP($ActionDmd) {
	global $Table,$Infos,$IdxPages;
	RecupFichiers($ActionDmd);
	$NbrPages=count($IdxPages);
	$idx=$ActionDmd[2]-1;
	$IdxDmd=$IdxPages[$idx];
	if ($ActionDmd[2] == $NbrPages) {
		$nbr=count($Table)-$IdxDmd;
	} else {
		$IdxFin=$IdxPages[$idx+1];
		$nbr=$IdxFin-$IdxDmd;
	}
	array_splice($Table,$IdxDmd,$nbr);
	array_splice($Infos,$IdxDmd,$nbr);
	EnregFichiers($ActionDmd);
	$ActionDmd[2]=$idx;
	Aff($ActionDmd);
}
// Inverser lignes et colonnes
function ILC(&$ActionDmd) {
	global $Table,$Infos,$IdxPages;
	RecupFichiers($ActionDmd);
	$ou=$ActionDmd[2]-1;
	$IdxPage=$IdxPages[$ou];
	$tmp=explode(",",$Infos[$IdxPage]);
	$tot=explode("-",$tmp[4]);
	$NbrCol=count($tot);
	$NbrPages=count($IdxPages);
	$Deb=$IdxPage+1;
	$Fin=$IdxPages[$ou+1];
	if ($ActionDmd[2] == $NbrPages) {
		$Fin=count($Table);
	}
	$NbrLignes=$Fin-$Deb;
	$Newtot="n";
	for ($i=1; $i<$NbrLignes; $i++) {
		$Newtot.="-n";
	}
	// pour les sommes des colonnes (raz)
	$tmp[4]=$Newtot;
	$Infos[$IdxPage]=implode(",",$tmp);
	$NewTable=array();
	$NewInfos=array();
	// inversion ligne colonne
	for ($i=0; $i<$NbrCol; $i++) {
		$NewLigne=array();
		$NewStyle=array();
		for ($j=$Deb; $j<$Fin; $j++) {
			$tmp1=explode(";",$Table[$j]);
			$NewLigne[]=$tmp1[$i];
			$tmp2=explode(";",$Infos[$j]);
			$NewStyle[]=$tmp2[$i];
		}
		$NewTable[$i]=implode(";",$NewLigne);
		$NewInfos[$i]=implode(",",$NewStyle);
	}
	// remplacement de la page
	array_splice($Table,$Deb,$NbrLignes);	
	array_splice($Infos,$Deb,$NbrLignes);
	array_splice($Table,$Deb,0,$NewTable);	
	array_splice($Infos,$Deb,0,$NewInfos);	
	EnregFichiers($ActionDmd);
	Aff($ActionDmd);
}
// Decaller une colonne
function DC(&$ActionDmd) {
	global $Table,$Infos,$IdxPages;
	RecupFichiers($ActionDmd);
	$ou=$ActionDmd[2]-1;
	$IdxPage=$IdxPages[$ou];
	$tmp=explode(",",$Infos[$IdxPage]);
	$tot=explode("-",$tmp[4]);
	$NbrCol=count($tot);
	$NbrPages=count($IdxPages);
	$Deb=$IdxPage+1;
	$Fin=$IdxPages[$ou+1];
	if ($ActionDmd[2] == $NbrPages) {
		$Fin=count($Table);
	}
	if ($ActionDmd[3] == "Av") {
		// colonne 0 à la fin
		for ($i=$Deb; $i<$Fin; $i++) {
			$tmp1=explode(";",$Table[$i]);
			$tmp2=explode(",",$Infos[$i]);
			$NewLigne="";
			$NewInfo="";
			$Newtot="";
			for ($j=1; $j<$NbrCol; $j++) {
				$NewLigne.=$tmp1[$j].";";
				$NewInfo.=$tmp2[$j].",";
				$Newtot.=$tot[$j]."-";
			}
			$Table[$i]=$NewLigne.$tmp1[0];
			$Infos[$i]=$NewInfo.$tmp2[0];
			$tmp[4]=$Newtot.$tot[0];
		}
	} else {
		if ($ActionDmd[4] == ($NbrCol-1)) {
			// dernière colonne au début
			for ($i=$Deb; $i<$Fin; $i++) {
				$tmp1=explode(";",$Table[$i]);
				$tmp2=explode(",",$Infos[$i]);
				$NewLigne=$tmp1[($NbrCol-1)];
				$NewInfo=$tmp2[($NbrCol-1)];
				$Newtot=$tot[($NbrCol-1)];
				for ($j=0; $j<($NbrCol-1); $j++) {
					$NewLigne.=";".$tmp1[$j];
					$NewInfo.=",".$tmp2[$j];
					$Newtot.="-".$tot[$j];
				}
				$Table[$i]=$NewLigne;
				$Infos[$i]=$NewInfo;
				$tmp[4]=$Newtot;
			}
		} else {
			for ($i=$Deb; $i<$Fin; $i++) {
				$tmp1=explode(";",$Table[$i]);
				$tmp2=explode(",",$Infos[$i]);
				$NewLigne="";
				$NewInfo="";
				$Newtot="";
				for ($j=0; $j<$NbrCol; $j++) {
					if ($j != $ActionDmd[4]) {
						$NewLigne.=$tmp1[$j];
						$NewInfo.=$tmp2[$j];
						$Newtot.=$tot[$j];
					} else {
						$NewLigne.=$tmp1[$j+1].";".$tmp1[$j];
						$NewInfo.=$tmp2[$j+1].",".$tmp2[$j];
						$Newtot.=$tot[$j+1]."-".$tot[$j];
						$j++;
					}
					if ($j != ($NbrCol-1)) {
						$NewLigne.=";";
						$NewInfo.=",";
						$Newtot.="-";
					}
				}
				$Table[$i]=$NewLigne;
				$Infos[$i]=$NewInfo;
				$tmp[4]=$Newtot;
			}
		}
	}
	$Infos[$IdxPage]=implode(",",$tmp);
	EnregFichiers($ActionDmd);
	Aff($ActionDmd);
}
// Decaller une page
function DP(&$ActionDmd) {
	global $Table,$Infos,$IdxPages;
	RecupFichiers($ActionDmd);
	$NbrPages=count($IdxPages);
	$ou=$ActionDmd[2]-1;
	$IdxDepart=$IdxPages[$ou];
	// selon où coller
	if ($ActionDmd[3] == "Av") {
		$IdxSuiv=$IdxPages[$ou+1];
		$nbr=$IdxSuiv-$IdxDepart;
		if ($ActionDmd[2] == 1) {
			// page 1 et avant ==> à la fin
			$IdxArrive=count($Table);
		} else {
			$IdxArrive=$IdxPages[$ou-1];
		}
	} else {
		if ($ActionDmd[2] == $NbrPages) {
			// derniere page et apres ==> au début 
			$nbr=Count($Table)-$IdxDepart;
			$IdxArrive=1;
		} else {
			$IdxSuiv=$IdxPages[$ou+1];
			$nbr=$IdxSuiv-$IdxDepart;
			if ($ActionDmd[2]== ($NbrPages-1)) {
				$nbr2=count($Table)-$IdxSuiv;
			} else {
				$IdxP2=$IdxPages[$ou+2];
				$nbr2=$IdxP2-$IdxSuiv;
			}
			$IdxArrive=$IdxDepart+$nbr2;
		}
	}
	// contrôle x
	$CopieTable=array_slice($Table,$IdxDepart,$nbr);
	$CopieInfos=array_slice($Infos,$IdxDepart,$nbr);
	array_splice($Table,$IdxDepart,$nbr);
	array_splice($Infos,$IdxDepart,$nbr);
	// contrôle v
	array_splice($Table,$IdxArrive,0,$CopieTable);
	array_splice($Infos,$IdxArrive,0,$CopieInfos);
	EnregFichiers($ActionDmd);
	array_splice($ActionDmd,3,2);
	Aff($ActionDmd);
}
// Inserer une page
function IP(&$ActionDmd) {
	global $Repert,$Table,$Infos,$IdxPages;
	RecupFichiers($ActionDmd);
	$NbrPages=count($IdxPages);
	$ou=$ActionDmd[2]-1;
	// selon où coller
	if ($ActionDmd[4] == "Av") {
		if ($ActionDmd[2] == 1) {
			// page 1 et avant ==> à la fin
			$IdxInsert=count($Table);
			$ActionDmd[2]=$NbrPages+1;
		} else {
			$IdxInsert=$IdxPages[$ou];
			$ActionDmd[2]-=1;
		}
	} else {
		if ($ActionDmd[2] == $NbrPages) {
			// derniere page et apres ==> au début 
			$IdxInsert=1;
			$ActionDmd[2]=1;
		} else {
			$IdxInsert=$IdxPages[$ou+1];
			$ActionDmd[2]+=1;
		}
	}
	if ($ActionDmd[3] == "AN") {
		// insertion ad nihilo
		$Titre=$_POST['Titre'];
		$NbrCol=$_POST['NbrCol'];
		if ($NbrCol == "") {$NbrCol=2;}
		CreePages(1,$NbrCol,$Titre,$IdxInsert);
		EnregFichiers($ActionDmd);
		Aff($ActionDmd);
	} else {
		// insertion par import
		if (move_uploaded_file($_FILES['import']['tmp_name'], $Repert.$_FILES['import']['name'])) {
			$nom=str_replace(".csv","",$_FILES['import']['name']);
			// sauvegarde du fichier existant
			$SaveAff=$ActionDmd[1];
			$SavePage=$ActionDmd[2];
			// récupération du fichier à inserer
			$ActionDmd[1]=$nom;
			$ActionDmd[2]=0;
			RecupFichiers($ActionDmd);
			for ($i=0; $i<count($Table); $i++) {
				$j=$i+1;
				$InsertTable[$i]=$Table[$j];
				$InsertInfos[$i]=$Infos[$j];
			}
			// suppression du fichier d'import
			$FichierCsv="$Repert".$ActionDmd[1].".csv";
			unlink($FichierCsv); 
			$FichierInf="$Repert".$ActionDmd[1].".inf";
			unlink($FichierInf);
			// réouverture du fichier initial et insertion
			$ActionDmd[1]=$SaveAff;
			$ActionDmd[2]=$SavePage;
			RecupFichiers($ActionDmd);
			array_splice($Table,$IdxInsert,0,$InsertTable);
			array_splice($Infos,$IdxInsert,0,$InsertInfos);
			EnregFichiers($ActionDmd);
			array_splice($ActionDmd,3,2);
			Aff($ActionDmd);
		} else {
			$MsgErr="Erreur lors de l'import du fichier";
			DIP($ActionDmd);
			return;
		}
	}
}
// Modification de page
function MP(&$ActionDmd) {
	global $Table,$Infos,$IdxPages;
	RecupFichiers($ActionDmd);
	$idx=$ActionDmd[2]-1;
	$IdxDmd=$IdxPages[$idx];
	$NbrPages=count($IdxPages);
	if ($ActionDmd[2] == $NbrPages) {
		$IdxFin=count($Table);
	} else {
		$IdxFin=$IdxPages[$idx+1];
	}
	$InfosPage=$Infos[$IdxDmd];
	$tmp=explode(",", $InfosPage);
	$tot=explode("-",$tmp[4]);
	$Table[$IdxDmd]=$_POST['Titre'];
	$Entete="N";
	if ($_POST['Entete']) {
		$Entete="O";
	}
	$Ligne="NP,".$_POST['Titre'].",".$_POST['Grouper'].",".$Entete;
	$Table[$IdxDmd]=$_POST['Titre'];
	// pour les modifs des colonnes sur toutes les lignes
	for ($i=($IdxDmd+1); $i< $IdxFin; $i++) {
		$Styles=explode(",",$Infos[$i]);
		$Valeurs=explode(";",$Table[$i]);
		$NewStyle="";
		$NewValeur="";
		$Totaux="";
		for ($j=0; $j<count($tot); $j++) {
			$colst="st-".$j;
			if ($_POST[$colst] == "") {
				$StyleCol=$Styles[$j];
			} else {
				$StyleCol=$_POST[$colst];
			}
			$colmc="mc-".$j;
			if ($_POST[$colmc] != "M") {
				$colpc="pc-".$j;
				$coltc="tc-".$j;
				if ($Totaux != "") {
					$NewStyle.=",";
					$NewValeur.=";";
					$Totaux.="-";
				}
				$NewValeur.=$Valeurs[$j];
				$NewStyle.=$StyleCol;
				$Totaux.=$_POST[$coltc];
				if ($_POST[$colpc] == "P") {
					$NewValeur.=";".$Valeurs[$j];
					$NewStyle.=",".$StyleCol;
					$Totaux.="-".$_POST[$coltc];
				}
			}
		}
		$Table[$i]=$NewValeur;
		$Infos[$i]=$NewStyle;
	}
	$Ligne.=",".$Totaux.",".$_POST['Style'].",".$_POST['CoulFond'].",".$_POST['CoulBord'].",".$_POST['Etendre'];	
	$Infos[$IdxDmd]=$Ligne;
	EnregFichiers($ActionDmd);
	Aff($ActionDmd);
}
// restauration des fichiers
function RD(&$ActionDmd) {
	global $Repert;
	$FichierCsv="$Repert".$ActionDmd[1].".csv";
	$Sauvegarde="$FichierCsv".".sav";
	copy($Sauvegarde, $FichierCsv); 
	$FichierInf="$Repert".$ActionDmd[1].".inf";
	$Sauvegarde="$FichierInf".".sav";
	copy($Sauvegarde, $FichierInf); 
	Aff($ActionDmd);
}
// sauvegarde des fichiers
function SD(&$ActionDmd) {
	global $Repert;
	$FichierCsv="$Repert".$ActionDmd[1].".csv";
	$Sauvegarde="$FichierCsv".".sav";
	copy($FichierCsv, $Sauvegarde);
	$FichierInf="$Repert".$ActionDmd[1].".inf";
	$Sauvegarde="$FichierInf".".sav";
	copy($FichierInf, $Sauvegarde);
	Aff($ActionDmd);
}
// suppression des fichiers
function SF(&$ActionDmd) {
	global $Repert;
	$FichierCsv="$Repert".$ActionDmd[1].".csv";
	unlink($FichierCsv); 
	$FichierInf="$Repert".$ActionDmd[1].".inf";
	unlink($FichierInf);
	$Sauvegarde="$FichierCsv".".sav";
	unlink($Sauvegarde);
	$Sauvegarde="$FichierInf".".sav";
	unlink($Sauvegarde);
	$ActionDmd[1]="Rien";
	Aff($ActionDmd);
}
// Modification des infos du fichier
function MF(&$ActionDmd) {
	global $Repert,$Menu,$Table,$Infos;
	RecupFichiers($ActionDmd);
	$Nom=Trim($_POST['Nom']);
	$Titre=$_POST['Titre'];
	$Style=$_POST['Style'];
	if ($Titre == "") {
		$Titre = "Fichier " . $Nom;
	}
	if ($Nom != $ActionDmd[1] && $Nom != "") {
		// on renomme le fichier
		$OldCsv=$Repert.$ActionDmd[1].".csv";
		$OldInf=$Repert.$ActionDmd[1].".inf";
		$NewCsv=$Repert.$Nom.".csv";
		$NewInf=$Repert.$Nom.".inf";
		rename($OldCsv,$NewCsv);
		rename($OldInf,$NewInf);
		// et les sauvegardes
		$OldSav=$OldCsv.".sav";
		$NewSav=$NewCsv.".sav";
		rename($OldSav,$NewSav);
		$OldSav=$OldInf.".sav";
		$NewSav=$NewInf.".sav";
		rename($OldSav,$NewSav);
	}
	$Infos[0]=$Titre.",".$Style;
	$Table[0]=$Titre;
	$ActionDmd[1]=$Nom;
	$ActionDmd[2]=0;
	EnregFichiers($ActionDmd);
	Aff($ActionDmd);
}
// Création du fichier
function CF(&$ActionDmd) {
	global $Table,$Infos,$MsgErr,$Repert;
	// Import de fichier
	if ($ActionDmd[1] == "Import") {
		if (move_uploaded_file($_FILES['import']['tmp_name'], $Repert.$_FILES['import']['name'])) {
			$nom=str_replace(".csv","",$_FILES['import']['name']);
			$ActionDmd[1]=$nom;
			$ActionDmd[2]=0;
			Aff($ActionDmd);
		} else {
			$MsgErr="Erreur lors de l'import du fichier";
			DCF($ActionDmd);
			return;
		}
		return;	
	}
	// Création ad nihilo
	if (isset($_POST['Nom'])) {
		$Nom=Trim($_POST['Nom']);
		$Titre=$_POST['Titre'];
		$NbrPages=$_POST['NbrPage'];
		$NbrCol=$_POST['NbrCol'];
	} else {
		$Nom=$ActionDmd[1];
		$Titre=""; 
		$NbrPages=1;
		$NbrCol=3;
	}
	if ($Nom == "") {
		$MsgErr="Intitulé pour le menu Obligatoire";
		DCF($ActionDmd);
		return;
	} else {
		$Fichier="$Repert".$Nom.".csv";
		if (file_exists($Fichier)) {
			$MsgErr="Fichier déjà existant dans le menu";
			DCF($ActionDmd);
			return;
		}
	}
	if ($Titre == "") {
		$Titre="Fichier : ".$Nom;
	}
	$Table[0]=$Titre;
	$Infos[0]=$Titre.",";
	// pour la page
	CreePages($NbrPages,$NbrCol,"Page",1);
	$ActionDmd[1]=$Nom;
	$ActionDmd[2]=0;
	EnregFichiers($ActionDmd);
	Aff($ActionDmd);
}
?>